//
//  APIMock.swift
//  OneAndOneTestWeatherAppTests
//
//  Created by Alexey Titov on 09.02.2021.
//

import Foundation
@testable import OneAndOneTestWeatherApp

class APIMock: API {
    var shouldSucceed = true
    var getWeatherForecastCalled = false
    var getIconURLCalled = false
    
    // MARK: - Mock data
    
    static var mockDate: Date = {
        var components = DateComponents()
        components.calendar = Calendar(identifier: .gregorian)
        components.year = 2021
        components.month = 2
        components.day = 9
        components.hour = 9
        return components.date ?? Date()
    }()
    
    var mockForecastItems: [ForecastItem] = [
        ForecastItem(
            dateTime: mockDate,
            main: ForecastItem.MainInfo(temperature: 5, pressure: 1000, humidity: 90),
            weather: [ForecastItem.Weather(main: "Clouds", icon: "02n")]
        )
    ]
    
    // MARK: - API
    
    func getWeatherForecast(completion: @escaping (Result<[ForecastItem], APIError>) -> Void) {
        getWeatherForecastCalled = true
        if shouldSucceed {
            completion(.success(mockForecastItems))
        } else {
            completion(.failure(.invalidJSON))
        }
    }
    
    func getIconURL(for iconName: String) -> URL {
        getIconURLCalled = true
        return URL(string: "http://openweathermap.org/img/wn/\(iconName)@2x.png")!
    }
}
