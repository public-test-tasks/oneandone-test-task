//
//  ForecastViewModelTests.swift
//  OneAndOneTestWeatherAppTests
//
//  Created by Alexey Titov on 09.02.2021.
//

import XCTest
@testable import OneAndOneTestWeatherApp

class ForecastViewModelTests: XCTestCase {
    var sut: ForecastViewModel!
    var apiMock: APIMock!

    override func setUp() {
        super.setUp()
        apiMock = APIMock()
    }

    override func tearDown() {
        apiMock = nil
        sut = nil
        super.tearDown()
    }

    func testGetWeatherForecastSuccess() {
        // Given
        apiMock.shouldSucceed = true
        
        // When
        sut = ForecastViewModel(apiClient: apiMock)
        
        // Then
        XCTAssertTrue(apiMock.getWeatherForecastCalled)
        XCTAssertEqual(sut.state, .loaded(sut.groupItemsByDay(items: apiMock.mockForecastItems)))
    }
    
    func testGetWeatherForecastFailure() {
        // Given
        apiMock.shouldSucceed = false
        
        // When
        sut = ForecastViewModel(apiClient: apiMock)
        
        // Then
        XCTAssertTrue(apiMock.getWeatherForecastCalled)
        XCTAssertEqual(sut.state, .error(.invalidJSON))
    }
}
