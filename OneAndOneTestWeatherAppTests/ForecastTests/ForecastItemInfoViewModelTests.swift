//
//  ForecastItemInfoViewModelTests.swift
//  OneAndOneTestWeatherAppTests
//
//  Created by Alexey Titov on 09.02.2021.
//

import XCTest
@testable import OneAndOneTestWeatherApp

class ForecastItemInfoViewModelTests: XCTestCase {
    var sut: ForecastItemInfoViewModel!
    var forecastItemMock: ForecastItem!
    var apiMock: APIMock!

    override func setUp() {
        super.setUp()
        apiMock = APIMock()
        forecastItemMock = apiMock.mockForecastItems[0]
        sut = ForecastItemInfoViewModel(item: forecastItemMock, apiClient: apiMock)
    }

    override func tearDown() {
        sut = nil
        apiMock = nil
        forecastItemMock = nil
        super.tearDown()
    }
    
    func testIconURL() {
        XCTAssertEqual(sut.iconURL, URL(string: "http://openweathermap.org/img/wn/02n@2x.png")!)
    }
    
    func testTemperature() {
        XCTAssertEqual(sut.temperature, "5.00 °C")
    }
    
    func testDate() {
        XCTAssertEqual(sut.date, "Feb 9, 2021")
    }
    
    func testTime() {
        XCTAssertEqual(sut.time, "9:00 AM")
    }
}
