//
//  AlertItem.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 27.01.2021.
//

import Foundation

struct AlertItem: Identifiable {
    var id = UUID()
    var title: String
    var message: String
}
