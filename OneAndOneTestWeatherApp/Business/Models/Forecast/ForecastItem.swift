//
//  ForecastItem.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

struct ForecastItem: Decodable {
    let dateTime: Date
    let main: MainInfo
    let weather: [Weather]
    
    private enum CodingKeys: String, CodingKey {
        case dateTime = "dt"
        case main
        case weather
    }
}

// MARK: - ForecastItem.MainInfo
extension ForecastItem {
    struct MainInfo: Decodable {
        let temperature: Float
        let pressure: Float
        let humidity: Float
        
        private enum CodingKeys: String, CodingKey {
            case temperature = "temp"
            case pressure
            case humidity
        }
    }
}

// MARK: - ForecastItem.Weather
extension ForecastItem {
    struct Weather: Decodable {
        let main: String
        let icon: String
    }
}
