//
//  ForecastErrorResponse.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

struct ForecastErrorResponse: Decodable {
    let message: String
}
