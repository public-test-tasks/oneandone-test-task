//
//  ForecastRow.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

struct ForecastRow: Identifiable {
    let id: Date
    let items: [ForecastItem]
}

extension ForecastRow: Equatable {
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
}
