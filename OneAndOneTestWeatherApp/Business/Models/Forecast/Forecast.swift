//
//  Forecast.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

struct Forecast: Decodable {
    let list: [ForecastItem]
}
