//
//  ForecastItemInfoView.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 27.01.2021.
//

import SwiftUI

struct ForecastItemInfoView: View {
    var viewModel: ForecastItemInfoViewModel
    
    var body: some View {
        VStack(spacing: 16) {
            Text(viewModel.date)
                .font(.system(.headline, design: .rounded))
            
            Text(viewModel.time)
                .font(.system(.subheadline, design: .rounded))
                .fontWeight(.medium)
            
            Text(viewModel.temperature)
                .font(.system(.title, design: .rounded))
                .fontWeight(.bold)
            
            Group {
                AsyncImage(
                    url: viewModel.iconURL,
                    placeholder: Image(systemName: "cloud.sun.rain")
                )
            }
            .padding(8)
            .frame(width: 80, height: 80)
        }
        .padding()
        .frame(width: 180, height: 240)
        .background(Color.gray.opacity(0.02))
        .cornerRadius(8)
        .shadow(radius: 2)
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .strokeBorder(lineWidth: 2)
                .foregroundColor(.gray)
                .opacity(0.5)
        )
    }
}
