//
//  ForecastView.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import SwiftUI

struct ForecastView: View {
    @ObservedObject var viewModel: ForecastViewModel
    
    var body: some View {
        NavigationView {
            VStack() {
                Picker("Mode", selection: $viewModel.mode) {
                    ForEach(ForecastViewModel.Mode.allCases, id: \.rawValue) { mode in
                        Text(mode.rawValue).tag(mode)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                
                contentView
                    .frame(maxHeight: .infinity)
            }
            .navigationBarTitle(LocalizedStringKey("Forecast"), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                viewModel.getWeatherForecast()
            }, label: {
                Image(systemName: "arrow.clockwise")
            }).disabled(viewModel.mode == .json))
            .alert(item: $viewModel.alertItem) { item -> Alert in
                Alert(title: Text(item.title), message: Text(item.message))
            }
        }
    }
    
    private var contentView: some View {
        switch viewModel.state {
        case .initial:
            return ProgressView(LocalizedStringKey("Loading...")).toAnyView()
        case .loaded(let rows):
            if rows.isEmpty {
                return noDataView.toAnyView()
            } else {
                return createForecastListView(rows: rows).toAnyView()
            }
        case .error:
            return retryButton.toAnyView()
        }
    }
    
    private func createForecastListView(rows: [ForecastRow]) -> some View {
        ScrollView(.vertical) {
            VStack(spacing: 0) {
                ForEach(rows) { row in
                    ForecastRowView(items: row.items)
                }
            }
        }
    }
    
    private var retryButton: some View {
        Button(action: {
            viewModel.getWeatherForecast()
        }, label: {
            VStack {
                Image(systemName: "repeat")
                Text(LocalizedStringKey("Retry"))
            }
            .padding()
        })
    }
    
    private var noDataView: some View {
        Text(LocalizedStringKey("No results found"))
    }
}
