//
//  ForecastRowView.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 27.01.2021.
//

import SwiftUI

struct ForecastRowView: View {
    var items: [ForecastItem]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(items, id: \.dateTime) { item in
                    ForecastItemInfoView(viewModel: ForecastItemInfoViewModel(item: item))
                }
            }
            .padding()
        }
    }
}
