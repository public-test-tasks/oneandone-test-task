//
//  ForecastItemInfoViewModel.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 27.01.2021.
//

import Foundation

class ForecastItemInfoViewModel: ObservableObject {
    let item: ForecastItem
    let apiClient: API
    
    var iconURL: URL {
        return apiClient.getIconURL(for: item.weather.first?.icon ?? "")
    }
    
    var date: String {
        return DateUtils.onlyDateFormatter.string(from: item.dateTime)
    }
    
    var time: String {
        return DateUtils.onlyTimeFormatter.string(from: item.dateTime)
    }
    
    var temperature: String {
        return String(format: "%.2f °C", item.main.temperature)
    }
    
    init(item: ForecastItem, apiClient: API = APIClient.shared) {
        self.item = item
        self.apiClient = apiClient
    }
}
