//
//  ForecastViewModel.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

class ForecastViewModel: ObservableObject {
    private let apiClient: API
    
    private var forecastRows: [ForecastRow] = []
    @Published private(set) var state: State = .initial
    @Published var alertItem: AlertItem?
    
    @Published var mode: Mode = .api {
        didSet {
            switchMode()
        }
    }
    
    init(apiClient: API = APIClient.shared) {
        self.apiClient = apiClient
        getWeatherForecast()
    }
    
    func switchMode() {
        switch mode {
        case .api:
            getWeatherForecast()
        case .json:
            fetchDataFromLocalJSON()
        }
    }
    
    func getWeatherForecast() {
        state = .initial
        apiClient.getWeatherForecast { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let items):
                strongSelf.forecastRows = strongSelf.groupItemsByDay(items: items)
                strongSelf.state = .loaded(strongSelf.forecastRows)
            case .failure(let error):
                strongSelf.state = .error(error)
                strongSelf.alertItem = AlertItem(title: "Error", message: error.message)
            }
        }
    }
    
    func fetchDataFromLocalJSON() {
        state = .initial
        // to show a loading view
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let forecast: Forecast = DataLoader.load("weather.json")
            self.forecastRows = self.groupItemsByDay(items: forecast.list)
            self.state = .loaded(self.forecastRows)
        }
    }
    
    func groupItemsByDay(items: [ForecastItem]) -> [ForecastRow] {
        guard items.count > 0 else {
            return []
        }
        
        var result: [ForecastRow] = []
        var rowItems: [ForecastItem] = []
        var currentDateGroup = items[0].dateTime.getDateOnly()
        
        for item in items {
            let dateOnly = item.dateTime.getDateOnly()
            if dateOnly == currentDateGroup {
                rowItems.append(item)
            } else {
                result.append(ForecastRow(id: currentDateGroup, items: rowItems))
                rowItems = [item]
                currentDateGroup = dateOnly
            }
        }
        
        result.append(ForecastRow(id: currentDateGroup, items: rowItems))
        return result
    }
}

// MARK: - ForecastViewModel.Mode
extension ForecastViewModel {
    enum Mode: String, CaseIterable {
        case api = "API request"
        case json = "Local JSON"
    }
}

// MARK: - ForecastViewModel.State
extension ForecastViewModel {
    enum State {
        case initial
        case loaded([ForecastRow])
        case error(APIError)
    }
}

extension ForecastViewModel.State: Equatable {
    static func ==(lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (.initial, .initial):
            return true
        case (.loaded(let lhsRows), .loaded(let rhsRows)):
            return lhsRows == rhsRows
        case (.error, .error):
            return true
        default:
            return false
        }
    }
}
