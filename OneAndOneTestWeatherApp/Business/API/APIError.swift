//
//  APIError.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

enum APIError: Error {
    case emptyResponse
    case invalidURL
    case invalidJSON
    case requestFailed(String)
    case forecastError(String)
    
    var message: String {
        switch self {
        case .emptyResponse:
            return "Empty Response"
        case .invalidURL:
            return "Invalid URL"
        case .invalidJSON:
            return "Invalid JSON"
        case .requestFailed(let message):
            return "Request failed with error: \(message)"
        case .forecastError(let message):
            return message
        }
    }
}
