//
//  API.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

protocol API {
    func getWeatherForecast(completion: @escaping (Result<[ForecastItem], APIError>) -> Void)
    func getIconURL(for iconName: String) -> URL
}
