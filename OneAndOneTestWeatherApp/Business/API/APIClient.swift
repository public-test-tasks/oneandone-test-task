//
//  APIClient.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import Foundation

final class APIClient {
    static let shared: APIClient = APIClient()
    
    private let baseURL: String
    private let session: URLSession = .shared
    
    private init(baseURL: String = Constants.baseURL) {
        self.baseURL = baseURL
    }
}

// MARK: - Constants
extension APIClient {
    struct Constants {
        static let baseURL = "http://api.openweathermap.org/data/2.5"
        static let city = "Munich"
        static let units = "metric"
        static let appID = "e9316b18a03ec3943e324cfd7e196eaf"
    }
}

// MARK: - API
extension APIClient: API {
    func getIconURL(for iconName: String) -> URL {
        return URL(string: "http://openweathermap.org/img/wn/\(iconName)@2x.png")!
    }
    
    func getWeatherForecast(completion: @escaping (Result<[ForecastItem], APIError>) -> Void) {
//        let endpoint = "/forecast?q=Munich&units=metric&appid=e9316b18a03ec3943e324cfd7e196eaf"
        let endpoint = "/forecast"
        var urlComponents = URLComponents(string: baseURL + endpoint)
        urlComponents?.queryItems = [
            URLQueryItem(name: "q", value: Constants.city),
            URLQueryItem(name: "units", value: Constants.units),
            URLQueryItem(name: "appid", value: Constants.appID)
        ]
        
        guard let url = urlComponents?.url else {
            completion(.failure(.invalidURL))
            return
        }
        
        let dataTask = session.dataTask(with: url) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(.requestFailed(error.localizedDescription)))
                }
                return
            }
            
            guard let jsonData = data else {
                DispatchQueue.main.async {
                    completion(.failure(.emptyResponse))
                }
                return
            }
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print("jsonString: \(jsonString)")
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let result = try decoder.decode(Forecast.self, from: jsonData)
                DispatchQueue.main.async {
                    completion(.success(result.list))
                }
            } catch {
                DispatchQueue.main.async {
                    if let errorResponse = try? JSONDecoder().decode(ForecastErrorResponse.self, from: jsonData) {
                        completion(.failure(.forecastError(errorResponse.message)))
                    } else {
                        completion(.failure(.invalidJSON))
                    }
                }
            }
        }
        
        dataTask.resume()
    }
}
