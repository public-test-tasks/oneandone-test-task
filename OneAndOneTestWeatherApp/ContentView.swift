//
//  ContentView.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ForecastView(viewModel: ForecastViewModel())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
