//
//  View+toAnyView.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 27.01.2021.
//

import SwiftUI

extension View {
    func toAnyView() -> AnyView {
        AnyView(self)
    }
}
