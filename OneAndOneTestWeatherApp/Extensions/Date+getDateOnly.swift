//
//  Date+getDateOnly.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 26.01.2021.
//

import Foundation

extension Date {
    func getDateOnly() -> Date {
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: dateComponents) ?? self
    }
}
