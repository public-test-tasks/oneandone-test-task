//
//  OneAndOneTestWeatherAppApp.swift
//  OneAndOneTestWeatherApp
//
//  Created by Alexey Titov on 23.01.2021.
//

import SwiftUI

@main
struct OneAndOneTestWeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
